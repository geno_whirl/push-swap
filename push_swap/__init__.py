# =====================
# Push Swap
# =====================
# Tomás Lungenstrass, 2022

"""
push_swap
---------

Push swap problem implementation.
"""

from abc import abstractmethod


class PushSwap:
    @property
    def a(self):
        return self._a

    @property
    def b(self):
        return self._b

    def __init__(self, a: list) -> None:
        assert all(isinstance(x, int) for x in a)  # assert int values
        assert len(a) == len(set(a))  # assert unique values

        self.move_count = 0
        self._a = a
        self._b = []

    def __repr__(self):
        return f"a = {self.a}\nb = {self.b}"

    def sa(self):
        """Swap first two elements in a."""

        if len(self.a) > 1:
            self.a[0], self.a[1] = self.a[1], self.a[0]
        self.move_count += 1

    def sb(self):
        """Swap first two elements in b."""

        if len(self.b) > 1:
            self.b[0], self.b[1] = self.b[1], self.b[0]
        self.move_count += 1

    def ss(self):
        """Swap first two elements within each of both piles."""

        if len(self.a) > 1:
            self.a[0], self.a[1] = self.a[1], self.a[0]
        if len(self.b) > 1:
            self.b[0], self.b[1] = self.b[1], self.b[0]
        self.move_count += 1

    def pa(self):
        """Push top element of b onto the top of a."""

        if len(self.b) > 0:
            self.a.insert(0, self.b.pop(0))
        self.move_count += 1

    def pb(self):
        """Push top element of a onto the top of b."""

        if len(self.a) > 0:
            self.b.insert(0, self.a.pop(0))
        self.move_count += 1

    def ra(self):
        """Move first element of a to bottom of pile."""

        if len(self.a) > 0:
            self.a.append(self.a.pop(0))
        self.move_count += 1

    def rb(self):
        """Move first element of b to bottom of pile."""

        if len(self.b) > 0:
            self.b.append(self.b.pop(0))
        self.move_count += 1

    def rr(self):
        """Move both first elements to bottom of their respective piles."""

        if len(self.a) > 0:
            self.a.append(self.a.pop(0))
        if len(self.b) > 0:
            self.b.append(self.b.pop(0))
        self.move_count += 1

    def rra(self):
        """Move last element of a to top of pile."""

        if len(self.a) > 0:
            self.a.insert(0, self.a.pop(-1))
        self.move_count += 1

    def rrb(self):
        """Move last element of b to top of pile."""

        if len(self.b) > 0:
            self.b.insert(0, self.b.pop(-1))
        self.move_count += 1

    def rrr(self):
        """Move both last elements to top of their respective piles."""

        if len(self.a) > 0:
            self.a.insert(0, self.a.pop(-1))
        if len(self.b) > 0:
            self.b.insert(0, self.b.pop(-1))
        self.move_count += 1

    def check_sorted(self):
        return len(self.b) == 0 and all(
            self.a[i] > self.a[i + 1] for i in range(len(self.a) - 1)
        )

    @abstractmethod
    def sort(self):
        assert len(self.b) == 0 and all(
            self.a[i] < self.a[i + 1] for i in range(len(self.a) - 1)
        )


class Strategy1(PushSwap):
    def sort(self):
        # if cyclical order is good, check if rr or rrr is faster
        # -> just order cyclically

        super().sort()

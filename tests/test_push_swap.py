"""
Contains all tests for push_swap.
"""

import random

from .context import push_swap

a = list(range(10))
random.shuffle(a)
print(a)

ps = push_swap.Strategy1(a=a)
print(ps)
